<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html  lang="ko">

<head>

	<!-- <link rel="stylesheet" href="../css/bootstrap.css"> 부트스트랩 -->

	<!-- GridStyle -->
	<style>
		.GMMainTable{border:0px; border-top:1px solid;}
		.GMCellHeader{border-right:0px; background:#f1f1f1 !important;color:#28374c;}
		.GMCell{border-right:0px; background-color:white !important; height:20px !important; padding-top:12px !important;}
		.HideCol0no, .HideCol0crtr_dt{text-align:center;}
		.GMCellHeader{height:20px !important; padding-top:10px !important;}
		.HideCol0title{padding-left:10px !important;}
		.pages > li{position:relative; left:50%;}
		.grdpms01{height:400px !important;}
		.right_body{min-height:650px; border-left:1px solid #cecece;}
		
		
/* 		 버튼 스타일 */
		.preserve-3d {
		    transform-style: preserve-3d;
		    -webkit-transform-style: preserve-3d;
		}
		
		.button_base {
		    font-size: 15px;
		    position: relative;
		    width: 60px;
		    height: 33px;
		    text-align: center;
		    box-sizing: border-box;
		    -webkit-box-sizing: border-box;
		    -moz-box-sizing: border-box;
		    -webkit-user-select: none;
		    cursor: default;
		}
		
		.button_base:hover {
		    cursor: pointer;
		}
		
		.b03_skewed_slide_in {
		    overflow: hidden;
		    border: #000000 solid 1px;
		}
		
		.b03_skewed_slide_in div {
		    position: absolute;
		    text-align: center;
		    width: 100%;
		    height: 50px;
		    box-sizing: border-box;
		    -webkit-box-sizing: border-box;
		    -moz-box-sizing: border-box;
		    padding: 10px;
		}
		
		.b03_skewed_slide_in div:nth-child(1) {
		    color: #000000;
		    background-color: #ffffff;
		}
		
		.b03_skewed_slide_in div:nth-child(2) {
		    background-color: #000000;
		    width: 230px;
		    transition: all 0.2s ease;
		    -webkit-transition: all 0.2s ease;
		    -moz-transition: all 0.2s ease;
		    transform: translate(-250px, 0px) skewX(-30deg);
		    -webkit-transform: translate(-250px, 0px) skewX(-30deg);
		    -moz-transform: translate(-250px, 0px) skewX(-30deg);
		}
		
		.b03_skewed_slide_in div:nth-child(3) {
		    color: #ffffff;
		    left: -200px;
		    transition: left 0.2s ease;
		    -webkit-transition: left 0.2s ease;
		    -moz-transition: left 0.2s ease;
		}
		
		.b03_skewed_slide_in:hover div:nth-child(2) {
		    transition: all 0.5s ease;
		    -webkit-transition: all 0.5s ease;
		    -moz-transition: all 0.5s ease;
		    transform: translate(-15px, 0px) skewX(-30deg);
		    -webkit-transform: translate(-15px, 0px) skewX(-30deg);
		    -moz-transform: translate(-15px, 0px) skewX(-30deg);
		}
		
		.b03_skewed_slide_in:hover div:nth-child(3) {
		    left: 0px;
		    transition: left 0.30000000000000004s ease;
		    -webkit-transition: left 0.30000000000000004s ease;
		    -moz-transition: left 0.30000000000000004s ease;
		}
		
		.button_base02 {
		    font-size: 15px;
		    position: relative;
		    left: 90%;
		    width: 70px;
		    height: 35px;
		    text-align: center;
		    box-sizing: border-box;
		    -webkit-box-sizing: border-box;
		    -moz-box-sizing: border-box;
		    -webkit-user-select: none;
		    cursor: default;
		    margin-top: 25px;
		}
		
		.button_base02:hover {
		    cursor: pointer;
		}



		
		#info_wrap tr > th{
			text-align: center;
			color :#ffffff ;
			font-size: 16px;
			/*background-color: rgb(68, 122, 192);*/
			background-color: #35699e;
		}

	
		#info_wrap tbody > tr{
			border-bottom: 1px solid lightgray;
			height: 40px;
			font-size: 15px;
		}

		#registerTable tbody > tr{
			border-bottom: 1px solid lightgray;
			height: 50px;
			font-size: 15px;
		}
		#modifyTable tbody > tr{
			border-bottom: 1px solid lightgray;
			height: 50px;
			font-size: 15px;
		}

		#info_wrap tbody > tr > td:nth-child(3){
			padding-left: 50px; 
			text-align : left;
		}

		#info_wrap tbody > tr:hover{
			background-color: #c6d9ec;
		}
	</style>
</head>

<%@include file="common.jsp"%>


<!-- TREEGRID -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/pms_gridutil.js"></script>

<script>
var editMode="";
var curPageNo;
var recordPerPage = 10;

var authority_check = "kihihi81@gmail.com";



/************************************************************\
* TreeGrid의 이벤트 처리 추가
* onClickPopup : 클릭시 수정창 popup
\************************************************************/
 function onClickPopup(info){
 	rowinfo = info;	//전역변수에 넣은 후 재사용
 	
 	var grid = info.grid;
     var data = info.data;
     var col_name = info.col_name;
     
     if(data && data.Kind == "Data" && info.data.GroupCol == null)	{
     	
     	batch_id = data.batch_id;
     	switch(col_name){
     		case "title":
     			editMode = "update";
     			fnDetail(data);
     			break;
     	}
     }
 }


function regNotice(){
	var reqMsg = XMLUtil.makeRequestXml();
	reqMsg = XMLUtil.addReqParam ('type', 'notice', reqMsg);
	reqMsg = XMLUtil.addReqParam ('board_seq', $("#notice_seq").val(), reqMsg);
	reqMsg = XMLUtil.addReqParam ('title', $("#notice_title").val(), reqMsg);
	reqMsg = XMLUtil.addReqParam ('contents', $("#notice_content").val(), reqMsg);
	
	AjaxUtil.callNCServiceByXML ("pp.BCBoardRetrieve#pmBoardReg", reqMsg, function(response) {
		var resultMessage = XMLUtil.getMessage(response);
		$(".showshow").hide();
		$("#border_cts").show();
		bindDataToListGrid('1');
		GeneralUtil.alert("Alert", resultMessage.messageName);
	});
}


function delNotice(){
	var reqMsg = XMLUtil.makeRequestXml();
	
	reqMsg = XMLUtil.addReqParam ('board_seq', $("#notice_seq").val(), reqMsg);
	
	AjaxUtil.callNCServiceByXML ("pp.BCBoardRetrieve#pmBoardDel", reqMsg, function(response) {
		var resultMessage = XMLUtil.getMessage(response);
		$(".showshow").hide();
		$("#border_cts").show();
		bindDataToListGrid('1');
		GeneralUtil.alert("Alert", "삭제 되었습니다.");
	});
}


function fnDetail(data){
	
	$("#border_cts").hide();
	$(".showshow").show();
	
	$("#notice_title").val("");
	$("#notice_crtr_dt").html("");
	$("#notice_content").val("");
	$("#notice_seq").val("");
	
	if(authority_check=="kihihi81@gmail.com"){
		$("#notice_title").attr("readonly", false);
		$("#notice_content").attr("readonly", false);
	}
	
	if ( editMode == "new" ){
		$("#notice_delete").hide();
	}
	else if (editMode == "update"){
		$("#notice_delete").show();
		
		var reqMsg = XMLUtil.makeRequestXml();
		reqMsg = XMLUtil.addReqParam ('board_seq', data.board_seq, reqMsg);
		AjaxUtil.callNCServiceByXML ("pp.BCBoardRetrieve#pmBoardInq", reqMsg, function(response) {
			var recArr =  XMLUtil.getRecordSetByArray ("rsOut", response);
			$("#notice_title").val(recArr[0].title);
			$("#notice_crtr_dt").html(recArr[0].crtr_dt);
			$("#notice_content").val(recArr[0].contents);
			$("#notice_seq").val(recArr[0].board_seq);
		})
	}
	
}


function setEnterKey(event) {
	var key = event.which || event.keyCode;
	if ( key == 13 ) {
		event.preventDefault();
		$("#bt_serch_list").trigger("click");
	}
}


$(function() {

	$("#notice_write").click(function(){
		editMode = "new";
		fnDetail();
	})
	
	$("#bt_serch_list").click(function(){
		bindDataToListGrid('1'); 
	});
	
	$('#bd_search').keypress(function(event) { setEnterKey(event); });
	
	$("#notice_submit").click(function(){
		regNotice();
	})
	
	$("#notice_delete").click(function(){
		delNotice();
	})
	
	$("#notice_list").click(function(){
		$(".showshow").hide();
		$("#border_cts").show();
	})
	
});



/***************************\
 * 전역변수 정의
\ **************************/ 
//const값. 안변함
var totalPage; //총 페이지 
var totalNotice; //공지사항 전부

//변할 수 있음
var comboNum; //몇개씩 보일건지
var nowPage; //현재 페이지
var showNotice; //보일 공지사항; 검색을 했으면 resultNotice가, 아니면 totalNotice가 보인다. 
var showTotalPage; //보일 전체 페이지. 검색을 했으면 그것에 맞게 바뀐다. parseInt(showNotice/combobox) + 1

var nowNoticeID = -1; //현재 내가 보고 있는 notice의 id
var nowNoticeIndex; //현재 내가 보고 있는 notice의 index


function showContent(noticeID){
	nowNoticeID = noticeID;
	nowNoticeIndex = getIndex(noticeID);

	$("#listNotice").css("display","none"); 
	$("#showNotice").css("display",'block');
	

	var notice = $.grep(totalNotice, function(value,index){
		return value["id"] == noticeID;
	})

	$("#noticeDetail  #title").text(notice[0].title);
	$("#noticeDetail  #date").text(notice[0].crtr_dt);
	$("#noticeDetail  #hit").text(notice[0].hit);
	$("#noticeDetail  #kind").text("<"+notice[0].kind+">");
	$("#noticeDetail  #contents").html(notice[0].content.replace(/\n/gi, "<br>"));
			


	$.ajax({
		type : "PATCH",
		url : "http://localhost:3000/notice/"+noticeID , // get the info from this url site
		data : {
			"hit" : Number(notice[0].hit) + 1
		},
		
		error : function(){alert("put error");},
		success : function(res){
			//alert(Number(notice[0].hit) + 1);
			

			//location.reload(true);
			
			$("#noticeDetail  #hit").text("조회수 : " + (Number(notice[0].hit) + 1));
			//getNotice();

		
		},
	});



	
}

function getIndex(target){
	//console.log(notice);
	// var index=0;
	for (var i = 0 ; i < showNotice.length;i++){
		if(showNotice[i].id == target){
			return i; // or i
		}
	}

	return null;
}

//정보를 받아 출력하는 함수, parameter?
function show(){ 
	var mkHtml = "";
	for(var i= 0; i < showNotice.length; i++) { //역순으로 출력 var i= showNotice.length; i >= 0; i--
		mkHtml+= "<tr style='display:' onclick = \"showContent("+showNotice[i].id+")\">";
		mkHtml += "<td>" + (i+1) +"</td>"; //순번 
		mkHtml += "<td>" + showNotice[i].kind + "</td>"; //구분 res[i].kind
		mkHtml += "<td>" + showNotice[i].title + "</td>"; //제목
		mkHtml += "<td>" + showNotice[i].hit + "</td>";
		mkHtml += "<td>" + showNotice[i].crtr_dt + "</td>" ;
		mkHtml += "</tr>";
	}

	$("#info_table  tbody").empty(); //원래있던 아이를 지움
	$('#info_table  tbody').append(mkHtml);  //qna_table 인 id를 가지는 table에 append


	//페이징
	

}

var _trcount;
//모든 공지의 내용을 가져온다
function getNotice(){ 
	nowPage = 1;
	
	$.ajax({
		method : "GET",
		url : "http://localhost:3000/notice", // get the data from this url server
		error : function(){alert("failed");}, //when error occurs
		success : function(res){
			_trcount = res.length
			showNotice=res; 
			//화면에 정보 보이는 함수
			show();

			console.log(getIndex(10));
			
			setPaging();


		}
		
	});
	

}

function setPaging(){

	rowsPerPage = 10, //div 그 자체\
	// alert(rowsPerPage);
	rows = $('#info_table > tbody > tr'),
	pageCount = _trcount/rowsPerPage,
	numbers = $('#numbers');

	for(var i = 1; i <= pageCount+1 ; i++){
		
        numbers.append('<li style="list-style-type: none; float: left; margin:10px"><a href= "">'+i+'</a></li>')
    }
    numbers.find('li:first-child a').addClass('active');

    displayRows(0);
            
	numbers.find('li').click(function(e){
		e.preventDefault();
		var index = $(this).index(); +1;
		// console.log(index);
		displayRows(index);
	});
}

function displayRows(idx){ // idx = 0
	var start = (idx) * rowsPerPage;
	var end = start + 10;
	rows.hide();
	rows.slice(start, end).show();
	// alert(start)
	// alert(end)
}
	
	// container.pagination({
	// 	items: _trcount, //data total count
	// 	itemsOnPage: 10, //1page max count
	// 	currentPage: nowPage, //current page
	// 	cssStyle: 'light-theme',
	// 	onPageClick: function(pageNum, event) {
	// 		event.preventDefault();
	// 		nowPage = pageNum;
	// 		show();
	// 	}
	// });






function saveRegister(){
	// $("#registerTable #title").val();
	// $("#registerTable #contents").val();

	
	// console.log($("#registerTable #title").val());
	// console.log($("#registerTable #kind").val());
	// console.log($("#registerTable #contents").val());

	$.ajax({
		type : "POST",
		url : "http://localhost:3000/notice", // get the data from this url server
		data :{
			//"id" : 30,
			
			"title" : $("#registerTable #title").val(),
			"kind" : $("#registerTable #kind").val(),
			"content" : $("#registerTable #contents").val(),


			"hit" : 0,
			"crtr_dt" : new Date().toISOString().substring(0,10),
			"crtr_id" : "myId",

			"upd_id" : "",
			"upd_dt" : "",

			



		},
		error : function(){alert("failed");}, //when error occurs
		success : function(res){
			alert("저장되었습니다");
			var id=1; 
			if(res.length > 0) {
				id = res[res.length -1].id +1;
			}
			location.reload(true);
			$("#writeNotice").css("display","none");
			$("#listNotice").css("display","block");
			getNotice();
		}
		
	});


}


function deleteNotice(){
	
	if (confirm("공지를 삭제하시겠습니까?") == true){
		$.ajax({
			type : "DELETE",
			url : "http://localhost:3000/notice/"+nowNoticeID ,
			error : function(){alert("delete failed");},
			// success : function(){console.log("delete success");}
		}).done(function(res){
			alert("삭제되었습니다");
			$("#showNotice").css("display","none");
			$("#listNotice").css("display","block");
			getNotice();
			
			nowNoticeID = -1; //사라졌으니 초기화..?}
		});
    }

}

function modifyNotice(){
	//alert("modify");


	var notice = $.grep(totalNotice,function(value,index){
		return value["id"] == nowNoticeID;
	});
	//console.log(notice[0]);
	$("#modifyTable #title").val(notice[0].title);
	$("#modifyTable #kind").val(notice[0].kind);
	$("#modifyTable #contents").text(notice[0].content); //since this input type is textarea not the text, in order to make the default value, need to use .text() method.

	//console.log(notice[0]);
	

	$("#modifyNotice #modify_btn").click(function(){

		//console.log(nowNoticeID);
		if(confirm("수정하시겠습니까?") == true){ //2. confirms alert pops up

			$.ajax({
			// need to get every variable in the text type variables ; just like the register button

				type : "PATCH",
				data : {
					"title" : $("#modifyTable #title").val(),
					"kind" : $("#modifyTable #kind").val(),
					//"content" : $("#write_content #content").val(),
					
					"content" : $("#modifyTable #contents").val(),	
					"upd_dt" : new Date().toISOString().substring(0,10),
					"upd_id" : "myid",

				},
				url :  "http://localhost:3000/notice/"+nowNoticeID , // get the info from this url site
				errror : function(){alert("modification failed");},

				success : function(res){
					//res.views 줄이기..? 수정도 지금 view가 증가하므로 ==> patch에서 손보면 될듯 함
					//$("#showNotice").css("display","none"); 
					$("#modifyNotice").css("display","none");
					
					alert("수정되었습니다");
					$.ajax({
						type : "GET",
						url : "http://localhost:3000/notice", // get the data from this url server
						error : function(){alert("failed");}, //when error occurs
						success : function(res){

							//전체 정보
							totalNotice = res; //const가 가능한가?
							
							//화면에 보일 공지들
							showNotice=totalNotice; //맨 처음에는 전체 공지 보여줌

							//새로 update된 함수
							
							//modifyNotice();
							var notice = $.grep(totalNotice, function(value,index){
								return value["id"] == nowNoticeID;
							})
							console.log(notice[0]);

							$("#noticeDetail  #title").text(notice[0].title);
							$("#noticeDetail  #date").text(notice[0].crtr_dt);
							$("#noticeDetail  #hit").text("조회수 : "+notice[0].hit);
							$("#noticeDetail  #kind").text("<"+notice[0].kind+">");
							$("#noticeDetail  #contents").html(notice[0].content.replace(/\n/gi, "<br>"));
							 
							$("#showNotice").css("display","block");
						}
						
					});
					
					
					
					//$("#listNotice").css("display","none"); 

					//$("#listNotice").css("display","none");
					//$("#showNotice").css("display","block");
					//창 none으로 보이게 하고 display show

				}


			});

				
		}
		

	});


	$("#modifyNotice #cancel_btn").click(function(){
		if(confirm("수정을 취소하시겠습니까?") == true){
			$("#modifyNotice").css("display","none"); 
			$("#showNotice").css("display","block"); 
				
		}

	});



}




$(document).ready(function(){
	$("#listNotice").css("display","block");
	//alert("dd");
	getNotice();

	
	$("#before_btn").click(function(){
		console.log(nowNoticeIndex);
		
		//index를 받아서 해당 index의 id를 nowNoticeID해서 showContent 하면 되는데 index어떻게 받지
		if(nowNoticeIndex == showNotice.length-1){
			alert("가장 처음 글입니다");
			//$("#before_btn").css("disable","disable");
		}
		else{
			//nowNoticeIndex += 1;
			nowNoticeID = showNotice[nowNoticeIndex + 1].id;

			showContent(nowNoticeID);
			
		}



	});

	$("#after_btn").click(function(){

		console.log(nowNoticeIndex);
		
		//index를 받아서 해당 index의 id를 nowNoticeID해서 showContent 하면 되는데 index어떻게 받지
		if(nowNoticeIndex == 0){
			alert("가장 마지막 글입니다");
			//$("#before_btn").css("disable","disable");
		}
		else{
			//nowNoticeIndex += 1;
			nowNoticeID = showNotice[nowNoticeIndex - 1].id;

			showContent(nowNoticeID);
			
		}

	});


	//공지 수정
	$("#showNotice #modify_btn").click(function(){
		$("#showNotice").css("display","none"); 
		$("#modifyNotice").css("display","block"); 
		modifyNotice();
	});

	
	//공지 삭제
	$("#showNotice #delete_btn").click(function(){
		deleteNotice(); //해당 공지 삭제. success 한 후 getNotice();
	});
	




	//글쓰기 버튼 누르기
	$("#listNotice #write_btn").click(function(){
		$("#listNotice").css("display","none"); 
		
		$("#writeNotice").css("display","block"); 


	});
	//등록 버튼 이벤트
	$("#writeNotice #register_btn").click(function(){
		saveRegister();
	});
	//목록으로가기 버튼 이벤트
	$("#writeNotice #goback_btn").click(function(){

		if (confirm("목록으로 돌아가시겠습니까?") == true){

			$("#writeNotice").css("display","none");
			$("#showNotice").css("display",'none');

			//nowPage=1; 
			show();
			$("#listNotice").css("display","block"); 
			
			
		}
		 

	});

	$("#showNotice #goback_btn").click(function(){

		$("#writeNotice").css("display","none");
		$("#showNotice").css("display",'none');

		//nowPage=1; 
		show();
		$("#listNotice").css("display","block"); 

	});
	


	//콤보박스의 값이 바뀌면
	

	// 검색 
	$("#search_btn").click(function(){
		search();
	});
	$("#search_keyword").keypress(function(e){
	
		if(e.keyCode==13){
			search();
			return false;
			
		}
	})



});







function search(){
	nowPage = 1;

	var keyword = $("#search_keyword").val();
	console.log(keyword=="");
	if(keyword == ""){
		alert("검색어를 입력하세요");
		showNotice = totalNotice;
		showTotalPage = totalPage;
		show();

		return false;
	} 


	var result_notice = $.grep(totalNotice,function(value, index){
		return value["title"].indexOf(keyword) >= 0;
	});
		
	showNotice = result_notice; //화면에 출력할 notice 목록을 바꾼다
	if (result_notice.length==0){ alert("검색결과가 없습니다");}
	//$("#search_keyword").val("");
	show(); //화면에 보여라
}






</script>


    <div class="main_div">
         
		<!-- include -->
		<!--top_menu-->		
		<%@include file="top.jsp"%>

		<div class="content_back">
			
			<div class="content_div">
		        
			    <div id="content">
				       
					<div id="left">
						<div class="left_top">
							<p>고객센터</p>
						</div>
						
						<div class="left_middle">
								<p><a href="#;" onclick="window.location.href='<%=request.getContextPath()%>/pmsplus/html/pp_notice.jsp'; return false;">공지사항</a></p>
						</div>			     
						<!-- <div class="left_middle2"> 
							<p><a href="#;" onclick="window.location.href='<%=request.getContextPath()%>/pmsplus/html/SMailQa.jsp'; return false;">가입문의</a></p> 
						</div>			       -->
						<!-- <div class="left_middle2">
							<p><a href="#;" onclick="window.location.href='<%=request.getContextPath()%>/pmsplus/html/pp_qanda.jsp'; return false;" >Q & A</a></p>
						</div>  
						<div class="left_middle2"> 
							<p><a href="#;" onclick="window.location.href='<%=request.getContextPath()%>/pmsplus/html/SMailQa.jsp'; return false;">FAQ</a></p> 
						</div> -->

					</div> <!--left-->
					
					<div class="right_body" >
						<div class="content_top"><!--content_top -->
							<div class="content_top_title" style="padding-left:10px">
								공지사항
								<span style="font-size:14px; color:#969696">PMSPLUS의  다양한 소식과 유용한 정보를 가장 빠르게 확인할 수 있습니다.</span>   
							</div>
					         <div class="content_top_line" >
					             <img src="../images/content_top_line_home.png"> > 고객센터  > <span>공지사항</span>
					         </div>
			      	  	</div> <!-- content_top -->
			     	   



							


			       		<div class="subcon_body">
							<div id = "listNotice" style="display : none;">
								<div id = "search " style="margin-left: 8px; padding : 30px 10px 0px 8px;">
									<form >
										<input id = "search_keyword"type = "text", placeholder="검색" style="width: 300px; height: 30px; border:1px solid black; font-size: 14px; float: left;">
										<button  id = "search_btn" type = "button" style="height: 30px;  font-size: 14px;">검색</button>
<%-- 	
										<select id = "comboNum" name = "num" style="margin-left: 10px; height: 30px; width: 80px; font-size: 14px;">
											<!-- <option value = 10>10개</option> -->
											<option value = 8>8개</option>
											<option value = 20>20개</option>
											<option value = 25>25개</option>
											<option value = 30>30개</option>
										</select> --%>
										<button id = "write_btn" type = "button" style = "width: 70px; height : 30px; font-size: 14px; float:right">글쓰기</button>
									</form>
								</div><!--search-->

								




								<div id = "info_wrap" style="margin-top: 20px; margin-left: 12px; margin-right: 10px;" >
									<table  class = "sub"  id = "info_table" style="width: 100%;">
										<colgroup>
											<col width = "100" >
											<col width = "70">
											<col width >
											<col width = "80">
											<col width = "120">
										</colgroup>

										<thead style="text-align: center; height: 40px; font-size: 14px;">
											<tr >
												<th scope="col">번호</th>
												<th scope="col">구분</th>
												<th scope="col">제목</th>
												<th scope="col">조회수</th>
												<th scope="col">날짜</th>
											</tr>
										</thead>

										<tbody style="text-align: center;">
											
										</tbody>
									
									</table>

								<nav aria-label="Page navigation example">
									<ul class="pagination pagination-seperated "></ul>
								</nav>
								<div id=pagingdiv >
									<div class="pagination" >
										<ol id="numbers" style="margin: 0 auto;">
										</ol>
									</div>
								</div>
								</div><!--info wrap-->

								<%-- <div id = "pageNumber" style="height: 30px; "></div> --%>

								

							</div><!--listNotice-->

							<div id = "showNotice" style="display: none; ">
								<div id = "noticeDetail" style="margin:10px 10px 10px 15px; border: 1px solid lightgray; ">
									
									<div style="margin: 10px 20px 10px 10px; border-bottom: 1px solid teal;padding-bottom: 5px;"> 
										<p id ="kind" style="display: inline; "></p>
										<p id = title style="display: inline; font-size: larger; padding-left: 30px;"></p>
										<p id = "hit" style="display: inline; font-size: smaller; float: right; padding-top: 10px;"></p>
										
									
									
									</div>
									<div style="margin: 10px 20px 10px 10px;">
										<p id = "contents"></p>
									
									</div>
									

									
									
								
								</div><!--noticeDetail-->
								
								<div id = buttons style="float: right; margin: 0px 10px 0px 0px;">
									<form >
										<button type = "button" id = "before_btn" >이전</button>
										<button type = "button" id = "modify_btn" >수정</button>
										<button type = "button" id = "delete_btn" >삭제</button>
										<button type = "button" id = "goback_btn">목록</button>
										<button type = "button" id = "after_btn" >다음</button>
									</form>
								</div>
							</div><!--showNotice-->

							<div id = "writeNotice" style = "display: none; border: 1px solid lightgrey; margin: 30px 12px 12px 20px; ">
								<div id = write_content >
									<table id="registerTable"  style="width:100%" >
										<colgroup>
											<col width = "100" >
											<col width = "70">
										</colgroup>

										<tbody>
											<tr scope="col">
												<th  style = "text-align: center; width: 400px;">제목</th>
												<th>
													<form>
														<input id = "title" type = "text" placeholder="제목을 입력하세요" style="width:500px">
													</form>
												</th>
											</tr>
						
											<tr>
												<th  style = "text-align: center;">구분</th>
												<th >
													<form>
														<select id = "kind" name = "num" style=" height: 30px; width: 80px; font-size: 14px;">
															<option value = "일반공지">일반공지</option>
															<option value = "긴급공지">긴급공지</option>
															<!-- <option value = "공지">공지</option> -->
														</select>
														<!-- <input id = "workplace" type = "text" placeholder="회사를 입력하세요" style="width:500px"> -->
													</form>
						
												</th>
											</tr>
						
						
											
						
											<tr scope="col" style="border-bottom : 0px;">
												<th style = "text-align: center;">공지내용</th>
												<th style="height: 100px;">
													<textarea id=  "contents" style="resize: none; margin: 10px 0px 0px 0px" name="Text1"  placeholder="내용을 입력하세요" cols="100" rows="30"></textarea>
													
												
												</th>
											</tr>
						
						
										</tbody>
									</table>
								</div> <!--write_content-->

								<div id = "buttons" style = " float :right; margin: 10px 0px 10px 0px;">
									<form >
										<button type = "button" id = "register_btn" >등록하기</button>
										<button type = "button" id = "goback_btn">목록으로</button>
									</form>

								</div> <!--buttons-->
							
							</div><!--writeNotice-->

							<div id = "modifyNotice" style = "display: none; border: 1px solid lightgrey; margin: 30px 12px 12px 20px; ">
								<div id = write_content >
									<table id="modifyTable"  style="width:100%" >
										<colgroup>
											<col width = "100" >
											<col width = "70">
										</colgroup>

										<tbody>
											<tr scope="col">
												<th  style = "text-align: center; width: 400px;">제목</th>
												<th>
													<form>
														<input id = "title" type = "text" placeholder="제목을 입력하세요" style="width:500px">
													</form>
												</th>
											</tr>
						
											<tr>
												<th  style = "text-align: center;">구분</th>
												<th >
													<form>
														<select id = "kind" name = "num" style=" height: 30px; width: 80px; font-size: 14px;">
															<option value = "일반공지">일반공지</option>
															<option value = "긴급공지">긴급공지</option>
															<!-- <option value = "공지">공지</option> -->
														</select>
														<!-- <input id = "workplace" type = "text" placeholder="회사를 입력하세요" style="width:500px"> -->
													</form>
						
												</th>
											</tr>
						
						
											
						
											<tr scope="col" style="border-bottom : 0px;">
												<th style = "text-align: center;">공지내용</th>
												<th style="height: 100px;">
													<textarea id=  "contents" style="resize: none; margin: 10px 0px 0px 0px" name="Text1"  placeholder="내용을 입력하세요" cols="100" rows="30"></textarea>
													
												
												</th>
											</tr>
						
						
										</tbody>
									</table>
								</div> <!--write_content-->

								<div id = "buttons" style = " float :right; margin: 10px 0px 10px 0px;">
									<form >
										<button type = "button" id = "modify_btn" >수정</button>
										<button type = "button" id = "cancel_btn">취소</button>
									</form>

								</div> <!--buttons-->
							
							</div><!--modifyNotice-->

							
						
							
								
						









						</div><!--subcon body-->







		    	    </div><!--right body-->
			    </div> <!--content -->
			    
			</div><!-- content_div -->
			  
		</div><!-- content_back -->
		   
	</div>  <!-- main_div -->
		 

<jsp:include page="bottom.jsp"></jsp:include>
 
 
 <!-- Grid Option -->
 <script>
 	function bindDataFromRecordSetPmsArr1(elHtml, recordSetId, data, colInfo, cbFunction, showFirstCheckBox, linkInfo, imageInfo, gridNo) {
		if(currentGridArr[gridNo]!=null) currentGridArr[gridNo].Dispose();		
		var rowCount = 0;			
		var cols = '';
		var leftCols = '';
		var contents = '';
		var headers = "<Header id='Header' NoEscape='1'"; //헤더 영역에 style적용위해
		var attrWidth;
		var attrCanEdit = '';
		var firstCheckBoxStr = '';
		if(showFirstCheckBox) firstCheckBoxStr = " Selecting='1'";
		var calColumn = "";
		
		for(var j=0; j<colInfo.length; j++){
			if(colInfo[j].id) {				
				if((colInfo[j].etc+"").indexOf('Width') >-1){
					attrWidth=" MinWidth='80' ";
				}else{
					attrWidth =" MinWidth='80' RelWidth='1'";
					//attrWidth =" RelWidth='1'";
				}
									
				//컬럼 수정가능 속성 : default=수정불가
				if((colInfo[j].etc+"").indexOf('Bool') >-1 || (colInfo[j].etc+"").indexOf('CanEdit') > -1){
					attrCanEdit="";
				}else{
					attrCanEdit=" CanEdit='0'";
				}
				if((colInfo[j].etc+"").indexOf('leftCols') >-1){
					leftCols += "<C Name='"+colInfo[j].id+"' "+ colInfo[j].etc + attrWidth + attrCanEdit +" />";
				}else{
					cols += "<C Name='"+colInfo[j].id+"' "+ colInfo[j].etc + attrWidth + attrCanEdit +" />";
				}
				headers += " " + colInfo[j].id + "='" + colInfo[j].name+"'";
				
				if (colInfo[j].Calc)
					calColumn +=  colInfo[j].Calc + ",";
			}
		}
		
		var navigation = XMLUtil.getPageNavigation("rsOut", data);
		var cnt = 0;
		XMLUtil.handleRecordSet (recordSetId, data, function(rec,hdr){
			
			var recData = new Object();
			for (i=0; i<rec.length; i++) {
				
				if(i==0) contents += "<I ";
				recData [hdr[i]] = rec[i];
				
				if(hdr[i] == "image"){
					contents += " "+ hdr[i] +"Icon='<%=request.getContextPath()%>/images/icon_magnifier.png' "+ hdr[i] +"IconAlign='center'";
				}
				else {
					contents += " "+ hdr[i] +"='"+rec[i]+"'";
				}

				
				//링크되는 컬럼에 스타일 추가
				if (linkInfo!=null && linkInfo[hdr[i]]!=null)
				{
					contents += " " + hdr[i] + "HtmlPrefix='&lt;div style=\"color:#35699e; font-weight: bold; cursor:pointer; \">'" + 
						" " + hdr[i] + "HtmlPostfix='&lt;/div>' ";
				}

				//이미지 삽입 컬럼에 이미지 추가
				if (imageInfo!=null && imageInfo[hdr[i]]!=null)
				{
					contents += " " + hdr[i] + "Button='<%=request.getContextPath()%>/images/icon_magnifier.png' ";
				}
				
				if(i+1 == rec.length)	{
					//no 추가
					contents += " no='"+(navigation.totalRecordCount - cnt)+"'";	
					contents +=" />";
				}

			}
			
			
			rowCount++;
			cnt++;

		});
		headers += " />";
        
		var header1 = "";

		if (calColumn.length > 0) calColumn = calColumn.substring(0, calColumn.length-1);
		var calDef = "<D Name='R' Calculated='1' CalcOrder='"+calColumn+"'/>";
		var gridDef = "<Def>"+calDef+"</Def><Panel Visible='0'/><Toolbar Visible='0'/><Solid></Solid>";
		
		var tgrid = "<Grid>"+gridDef+"<Cfg Code='GTBEZCPSIIQIKC' " +firstCheckBoxStr+ "  MaxWidth='1' NoVScroll='1' ConstWidth='0' /><LeftCols>"+leftCols+"</LeftCols><Cols>"+cols+"</Cols><Body><B>"+contents+"</B></Body><Head>"+ headers + header1 + "</Head></Grid>";
		
		
		currentGridArr[gridNo] = new TreeGrid(tgrid, jQuery(elHtml).attr('id'));

		//클릭시 수행되는 이벤트를 cbFunction으로 전환
		if(cbFunction){
			currentGridArr[gridNo].Actions.OnClick = function(targetgrid, data, value, event){
				return cbFunction({grid:targetgrid, data:data, col_name: value, event:event});
			}
		}
	}
 	
 	//$(".content_div , #left").css("height","850px");
 </script>